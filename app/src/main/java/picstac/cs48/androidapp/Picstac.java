package picstac.cs48.androidapp;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

import picstac.cs48.androidapp.posts.MystacManager;
import picstac.cs48.androidapp.posts.NewPostsManager;
import picstac.cs48.androidapp.posts.PopularPostsManager;
import picstac.cs48.androidapp.posts.PostManager;
import picstac.cs48.androidapp.posts.UploadManager;
import picstac.cs48.androidapp.utils.MyStacUtility;
import uk.co.senab.bitmapcache.BitmapLruCache;

/**
 * Created by nick on 1/24/15.
 *
 * Application class stores global information that can be accessed by any Activity within the app.
 * Follows the singleton design pattern.
 */
public class Picstac extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static Picstac singleton;
    private static final String cacheName = "/picstac-bitmap-cache";
    private BitmapLruCache mCache;
    private File cacheLocation;
    private ConcurrentHashMap<String, PostManager> pmMap;
    private LocationManager cachedStartupLocation;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;

    public static String android_id;

    public static Picstac getInstance(){
        return singleton;
    }

    /**
     * Called when the application is starting, before any activity, service,
     * or receiver objects (excluding content providers) have been created.
     * Implementations should be as quick as possible (for example using
     * lazy initialization of state) since the time spent in this function
     * directly impacts the performance of starting the first activity,
     * service, or receiver in a process.
     * If you override this method, be sure to call super.onCreate().
     */
    @Override
    public void onCreate() {
        super.onCreate();

        singleton = this;

        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        // initialize DEPRECATED_LocationProvider, uses default refresh time
        cachedStartupLocation = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mLocation = cachedStartupLocation.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        // Create a directory for the on-disk cache
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            cacheLocation = new File(
                    Environment.getExternalStorageDirectory() + cacheName);
        } else {
            cacheLocation = new File(getFilesDir() + cacheName);
        }
        cacheLocation.mkdirs();

        // Enable memory and disk cache, set disk cache storage location
        BitmapLruCache.Builder builder = new BitmapLruCache.Builder(this);

        // Allocate half of the heap memory to the memcache
        builder.setMemoryCacheEnabled(true).setMemoryCacheMaxSizeUsingHeapSize(.5f);
        builder.setDiskCacheEnabled(true).setDiskCacheLocation(cacheLocation);

        // Create the global cache
        mCache = builder.build();

        // Load the list of ids for MyStac
        new Thread(new Runnable() {
            @Override
            public void run() {
                MyStacUtility.read();
            }
        }).start();

        // Set up PostManagers hash map
        pmMap = new ConcurrentHashMap<String, PostManager>();
        pmMap.put("new", new NewPostsManager());
        pmMap.put("popular", new PopularPostsManager());
        pmMap.put("uploads", new UploadManager());
        pmMap.put("mystac", new MystacManager());

        buildGoogleApiClient();
    }

    public File getBitmapCacheDir() {
        return this.cacheLocation;
    }

    public BitmapLruCache getBitmapCache() {
        return mCache;
    }

    public void reloadAllFeeds() {
        for (String key : pmMap.keySet()) {
            pmMap.get(key).reloadPosts();
        }
    }

    /**
     * Returns a reference to a PostManager.
     * @param key The string name of the PostManager (e.g. "new", "popular").
     * @return Reference to the PostManager being requested.
     */
    public PostManager getPostManager(String key) { return pmMap.get(key); }

    public static Picstac getApplication(Context context) {
        return (Picstac) context.getApplicationContext();
    }

    /**
     * Initializes the GoogleAPIClient in order to provide connectivity to Google Play Services
     */
    private synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
        mGoogleApiClient.connect();
    }

    /**
     * Callback from GoogleAPIClient initialization
     * @param bundle the saved state of the activity
     */
    @Override
    public void onConnected(Bundle bundle) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.d("Google FusedLocationAPI", "Lat: "+mLocation.getLatitude()+" Lon: "+mLocation.getLongitude());

        pmMap.get("new").updateGeolocation(mLocation.getLatitude(), mLocation.getLongitude());
        pmMap.get("popular").updateGeolocation(mLocation.getLatitude(), mLocation.getLongitude());
    }

    /**
     * Start Google Play Services geolocation tracking
     */
    public void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, createLocationRequest(), this);
}
    /**
     * Stop Google Play Services geolocation tracking
     */
    public void stopLocationUpdates(){
        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        } catch (IllegalStateException ise) {
            ise.printStackTrace();
        }
    }

    /**
     * Initializes the LocationRequest for the GoogleAPIClient
     * @return the LocationRequest
     */
    private LocationRequest createLocationRequest(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000*60);  //1000*x Seconds
        mLocationRequest.setFastestInterval(1000*60);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            //TODO Handle failed connection
    }

    /**
     * Updates the feeds when Google Play Services detects a location change
     * @param location the new location
     */
    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        pmMap.get("new").updateGeolocation(mLocation.getLatitude(), mLocation.getLongitude());
        pmMap.get("popular").updateGeolocation(mLocation.getLatitude(), mLocation.getLongitude());
    }

    public GoogleApiClient getGoogleApiClient() {
        return this.mGoogleApiClient;
    }

    public Location getLocation() {
        return mLocation;
    }
}
