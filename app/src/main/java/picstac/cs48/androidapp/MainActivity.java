package picstac.cs48.androidapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.File;

import picstac.cs48.androidapp.feed.FeedFragment;
import picstac.cs48.androidapp.navigationdrawer.NavigationDrawerCallbacks;
import picstac.cs48.androidapp.navigationdrawer.NavigationDrawerFragment;
import picstac.cs48.androidapp.slidingtab.SlidingTabLayout;
import picstac.cs48.androidapp.utils.MyStacUtility;
import picstac.cs48.androidapp.utils.TrimCacheMemoryThread;

/**
 * Main activity that the user will see when they open the app.
 */
public class MainActivity extends ActionBarActivity implements FeedFragment.OnFragmentInteractionListener, NavigationDrawerCallbacks{
    private Toolbar toolbar;
    private NavigationDrawerFragment mNavigationDrawerFragment;

    ViewPager viewPager;
    MainActivityPagerAdapter mAdapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"New", "Popular", "MyStac", "Uploads"};
    int Numoftabs = 4;

    private static final int PLAY_SERVICE_FAILURE = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // TODO Trying to set type face crashes app FIXXXX
        //Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/DroidSans.ttf");
        //TextView tv = (TextView) findViewById(R.id.card_caption);
        //tv.setTypeface(tf);

        // TODO Trying to set type face crashes app FIXXXX
        //Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/DroidSans.ttf");
        //TextView tv = (TextView) findViewById(R.id.card_caption);
        //tv.setTypeface(tf);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // TODO setting the logo inflates it to the entire screen, FIX
        // toolbar.setLogo(R.drawable.starterlogo);

        mAdapter = new MainActivityPagerAdapter(getSupportFragmentManager(), Titles, Numoftabs);

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(mAdapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorDarkPrimary);
            }
        });

        tabs.setViewPager(viewPager);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.action_camera:
                startCamera();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        Log.e("MainActivity", "Main Activity stopping!");

        // Delete files in image cache
        try {
            for (File f : Picstac.getInstance().getBitmapCacheDir().listFiles()) {
                if(!f.delete()) {
                    Log.e("MainActivity", "Unable to delete file: " + f.toString());
                }
            }
            if(!Picstac.getInstance().getBitmapCacheDir().delete()) {
                Log.e("MainActivity", "Unable to delete directory: " + Picstac.getInstance().getBitmapCacheDir().toString());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        // Save mystac file
        MyStacUtility.save();

        super.onStop();
    }

    @Override
    public void onResume(){
        super.onResume();
        /*Verify Google Play Services*/
        checkGooglePlayServices();
        if(Picstac.getInstance().getGoogleApiClient().isConnected()){
            Picstac.getInstance().startLocationUpdates();
        }

    }
    @Override
    public void onPause() {
        super.onPause();
        Picstac.getInstance().stopLocationUpdates();
    }

    private  boolean checkGooglePlayServices(){
        int res = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(res == ConnectionResult.SUCCESS){
            return true;
        }
        GooglePlayServicesUtil.showErrorDialogFragment(res, this, PLAY_SERVICE_FAILURE);
        return false;
    }

    @Override
    public void onTrimMemory(int level) {
        if (level == TRIM_MEMORY_RUNNING_LOW || level == TRIM_MEMORY_RUNNING_CRITICAL
                || level == TRIM_MEMORY_UI_HIDDEN) {
            new TrimCacheMemoryThread().start();
            Log.e("MainActivity", "Low memory!!!");
        }
        super.onTrimMemory(level);
    }

    /**
     * Starts the DisplayImage activity when you click the camera button.
     */
    public void startCamera() {
        Intent intent = new Intent(this, CameraActivity.class);
        this.startActivity(intent);
    }

    @Override
    public void hideToolBarOnScroll(boolean hideToolBar) {
        if(hideToolBar){
            getSupportActionBar().hide();
            tabs.setViewPager(null);

        }
        else{
            getSupportActionBar().show();
            tabs.setViewPager(viewPager);
        }
    }
    /**
     * Handles logic for navigation drawerframgment
     *@param position position of selected navigationitem
     **/
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        //Toast.makeText(this, "Menu item selected -> " + position, Toast.LENGTH_SHORT).show();
        Log.d("Map activity INTENT", String.valueOf(position));
        if(position == 0){
            try {
                Log.d("Map activity INTENT", "GO");
                Intent intent = new Intent(this, MapActivity.class);
                this.startActivity(intent);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    //Close nav drawer on back press
    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }
}
