package picstac.cs48.androidapp.posts;

import android.util.Log;

import java.util.NoSuchElementException;
import java.util.Vector;

import picstac.cs48.androidapp.utils.ClearCacheThread;

/**
 * Created by Nick on 1/26/2015.
 * PostManager that handles new posts feed.
 */
public class NewPostsManager extends PostManager implements FetchNewPostsRunnable.PostsFetchedEvent, ClearCacheThread.ClearCacheEvent {

    @Override
    public String getFeedType() {
        return "new";
    }

    /**
     * Returns a Post from specified index in the posts array
     * @param index - Index of Post in posts array
     * @return      - Post object
     */
    @Override
    public Post getPost(int index) {
        postCursor = (index > postCursor) ? index : postCursor;
        Log.d("NewPostsManager", "Cursor index is " + postCursor);

        // If the distance between the view cursor and number of posts is
        // less than or equal to the number of posts we fetch from the
        // server at a time, fetch more posts.
        if ((posts.size() - postCursor) <= POST_FETCH_BATCH_SIZE) {
            fetchMorePosts();
        }

        return posts.get(index);
    }

    /**
     * Starts a new fetchPosts runnable Thread
     */
    public void fetchMorePosts() {
        Log.d("NewPostsManager", "fetchMorePosts called with coords (" + latitude + "," + longitude + ")");
        if (!threadRunning) {
            threadRunning = true;
            postFetchThreadRef = new Thread(new FetchNewPostsRunnable(this, latestID, this.latitude, this.longitude));
            postFetchThreadRef.start();
        }
    }

    /**
     * Callback called by FetchNewPostsRunnable when it fetches new posts
     * @param newPosts  - A Vector of the new posts the runnable fetched from the server
     */
    @Override
    public void postsFetchedEvent(Vector<Post> newPosts) {
        try {
            Log.d("NewPostsManager", "Posts were fetched!  Latest ID is " + newPosts.lastElement().getId());
            latestID = newPosts.lastElement().getId();
            this.addPost(newPosts);
            threadRunning = false;
            retries = 0;
        } catch (NoSuchElementException e) {
            Log.e("NewPostsManager", "PostsFetchedEvent contained an empty array!");

            // Try again
            threadRunning = false;
            retries++;

            if (retries > 10) {
                return;
            }

            fetchMorePosts();
        }
    }
}
