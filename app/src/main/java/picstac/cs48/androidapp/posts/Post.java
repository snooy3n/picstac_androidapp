package picstac.cs48.androidapp.posts;

/**
 * Created by Nick on 1/22/2015.
 * Encapsulates a PicStac post (image key, comments, vote stats, etc.)
 */
public class Post {
    public static final int VOTE_UP = 1;
    public static final int VOTE_DOWN = -1;
    public static final int VOTE_NONE = 0;

    // Instance variables for each Post object
    private String imageUrl;
    private int votes;
    private double[] location;
    private String id, caption;
    private int userVote;

    /**
     * Initializes all private class variables to zero or its equivalent
     */
    public Post() {
        this.imageUrl = "";
        this.votes = 0;
        this.location = new double[2];
        this.location[0] = 0.0;
        this.location[1] = 0.0;
        this.id = "";
        this.caption = "";
        this.userVote = VOTE_NONE;
    }

    /**
     * Initializes a new post with specified fields
     * @param url       - URL of the image associated with this post
     * @param votes     - Number of votes this post has
     * @param latitude  - The latitude of this post (double)
     * @param longitude - The longitude of this post (double)
     */
    public Post(String url, int votes, double latitude, double longitude, String id, String caption, int userVote) {
        this.imageUrl = url;
        this.votes = votes;
        this.location = new double[2];
        this.location[0] = latitude;
        this.location[1] = longitude;
        this.id = id;
        this.caption = caption;
        this.userVote = userVote;
    }

    public String getUrl() {
        return this.imageUrl;
    }

    public int getVotes() {
        return this.votes;
    }

    public void setVotes(int newVote) { this.votes = newVote; }

    public double getLongitude() {
        return this.location[1];
    }

    public double getLatitude() {
        return this.location[0];
    }

    public String getId() { return this.id; }

    public String getCaption() { return this.caption; }

    public void setUserHasVoted(int v) { this.userVote = v; }

    public int getUserVote() { return this.userVote; }
}
