package picstac.cs48.androidapp.posts;

import android.util.Log;

import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * Created by Nick on 2/14/2015.
 * PostManager that handles the popular posts feed.
 */
public class PopularPostsManager extends PostManager implements FetchNewPostsRunnable.PostsFetchedEvent{
    @Override
    public String getFeedType() {
        return "popular";
    }

    /**
     * Fetches more posts from the server.
     */
    @Override
    public void fetchMorePosts() {
        Log.d("PopularPostsManager", "fetchMorePosts called with coords (" + latitude + "," + longitude + ")");
        if (!threadRunning) {
            threadRunning = true;
            postFetchThreadRef = new Thread(new FetchPopularPostsRunnable(latitude, longitude, this));
            postFetchThreadRef.start();
        }
    }

    /**
     * Callback called by FetchNewPostsRunnable when it fetches new posts
     * @param newPosts  - A Vector of the new posts the runnable fetched from the server
     */
    @Override
    public void postsFetchedEvent(Vector<Post> newPosts) {
        try {
            Log.d("PopularPostsManager", "Posts were fetched!");
            this.addPost(newPosts);
            threadRunning = false;
            retries = 0;
        } catch (NoSuchElementException e) {
            Log.e("PopularPostsManager", "PostsFetchedEvent contained an empty array!");

            // Try again
            threadRunning = false;
            retries++;

            if (retries > 10) {
                return;
            }

            fetchMorePosts();
        }
    }
}
