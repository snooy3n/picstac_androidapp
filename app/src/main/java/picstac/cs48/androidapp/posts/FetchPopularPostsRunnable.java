package picstac.cs48.androidapp.posts;

import java.util.Vector;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;

/**
 * Created by Nick on 2/14/2015.
 * Runnable that will fetch popular posts from the server.
 */
public class FetchPopularPostsRunnable extends FetchNewPostsRunnable {

    private final String queryBase = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.popular_api);
    private final int POPULAR_POSTS_BATCH_SIZE = 15;
    private final double latitude, longitude;

    public FetchPopularPostsRunnable(double latitude, double longitude, PostsFetchedEvent callback) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.posts = new Vector<Post>(POPULAR_POSTS_BATCH_SIZE);
        this.callback = callback;
    }

    @Override
    public void run() {
        // Set thread to run in background priority
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        // Query server
        String query = queryBase + "lat=" + latitude + "&lon=" + longitude;
        this.requestPostsFromServer(query);
    }
}
