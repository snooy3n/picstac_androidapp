package picstac.cs48.androidapp.posts;

import android.util.Log;

import java.util.NoSuchElementException;
import java.util.Vector;

import picstac.cs48.androidapp.Picstac;

/**
 * Created by Nick on 2/26/2015.
 * PostManager that handles the feed of posts that a user has uploaded.
 */
public class UploadManager extends PostManager
        implements FetchUploadedPostsRunnable.FetchUploadPostsEvent {
    @Override
    public String getFeedType() {
        return "uploads";
    }

    /**
     * Fetches more posts from the server.
     */
    @Override
    public void fetchMorePosts() {
        Log.d("UploadManager", "fetchMorePosts called with coords (" + latitude + "," + longitude + ")");
        if (!threadRunning) {
            threadRunning = true;
            postFetchThreadRef = new Thread(new FetchUploadedPostsRunnable(Picstac.android_id, this));
            postFetchThreadRef.start();
        }
    }

    /**
     * Callback called by FetchMystacPosts when it fetches new posts
     * @param newPosts  - A Vector of the new posts the runnable fetched from the server
     */
    @Override
    public void fetchMystacPostsEvent(Vector<Post> newPosts) {
        try {
            Log.d("UploadManager", "Posts were fetched!  Latest ID is " + newPosts.lastElement().getId());
            this.addPost(newPosts);
            threadRunning = false;
            retries = 0;
        } catch (NoSuchElementException e) {
            Log.e("UploadManager", "PostsFetchedEvent contained an empty array!");

            // Try again
            threadRunning = false;
            retries++;

            if (retries > 10) {
                return;
            }

            fetchMorePosts();
        }
    }
}
