package picstac.cs48.androidapp.posts;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;

/**
 * Created by Nick on 2/26/2015.
 * Runnable that will fetch the user's uploaded posts from the server.
 */
public class FetchUploadedPostsRunnable implements Runnable {
    public interface FetchUploadPostsEvent {
        public void fetchMystacPostsEvent(Vector<Post> posts);
    }

    private String id;
    private Vector<Post> posts;
    private FetchUploadPostsEvent callback;
    private String queryBase = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.user_api);

    public FetchUploadedPostsRunnable(String id, FetchUploadPostsEvent callback) {
        this.id = id;
        this.callback = callback;
        this.posts = new Vector<Post>();
    }

    @Override
    public void run() {
        try {
            if (id.equals("")) throw new RuntimeException("ID must be assigned!");

            String query = queryBase + this.id;

            HttpURLConnection conn = (HttpURLConnection) new URL(query).openConnection();
            conn.setRequestProperty("Accept", "application/json");
            InputStream is = conn.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is), 1024);

            for (String line = r.readLine(); line != null; line = r.readLine()) {
                sb.append(line);
            }

            try {
                is.close();
            } catch (IOException e) {
                // There should be no issues closing the input stream
                Log.e("FetchMSPostsRunnable", e.toString());
            }

            String response = sb.toString();

            // Get array of post data
            JSONArray document = new JSONArray(response);

            // Iterate through and add data to a new post object
            for (int i = 0; i < document.length(); i++) {
                JSONObject p = document.getJSONObject(i);
                Post newPost = new Post(p.getString("url"), p.getInt("score"), p.getDouble("lat"),
                        p.getDouble("lon"), p.getString("_id"), p.getString("caption"), p.getInt("vote"));
                Log.d("FetchMSPostsRunnable", "Loaded new post with caption: " + newPost.getCaption() + " and ID: " + newPost.getId());

                this.posts.add(newPost);
            }

            try {
                callback.fetchMystacPostsEvent(this.posts);
            } catch (NullPointerException npe) {}
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
