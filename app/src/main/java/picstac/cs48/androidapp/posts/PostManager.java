package picstac.cs48.androidapp.posts;

import android.util.Log;

import java.util.Observable;
import java.util.Vector;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.utils.ClearCacheThread;


/**
 * Created by Nick on 2/14/2015.
 * Abstract class that manages posts belonging to a variety of different feeds.
 */
public abstract class PostManager extends Observable
        implements ClearCacheThread.ClearCacheEvent {
    public static final int POST_FETCH_BATCH_SIZE = 5;

    protected Vector<Post> posts;
    protected int postCursor, retries;
    protected boolean threadRunning;
    protected String latestID;
    protected Vector observers;
    protected Thread postFetchThreadRef;
    protected double latitude, longitude;

    public PostManager() {
        posts = new Vector<Post>(POST_FETCH_BATCH_SIZE);
        postCursor = 0;
        retries = 0;
        threadRunning = false;
        latestID = "";
        try {
            this.latitude = Picstac.getInstance().getLocation().getLatitude();
            this.longitude = Picstac.getInstance().getLocation().getLongitude();
        } catch (Exception e) {
            this.latitude = 0.0;
            this.longitude = 0.0;
        }
        observers = new Vector();
    }

    /**
     * Returns what kind of feed this PostManager is handling.
     * @return The kind of feed.
     */
    public abstract String getFeedType();

    /**
     * Reloads the posts handled by this manager.
     */
    public void reloadPosts() {
        if (threadRunning) {
            postFetchThreadRef.interrupt();
        }
        threadRunning = false;
        latestID = "";
        postCursor = 0;
        ClearCacheThread ccThread = new ClearCacheThread(this, Picstac.getInstance().getBitmapCache(), this);
        ccThread.start();
    }

    public void clearCacheEvent(boolean successful) {
        if (successful) {
            Log.d(getFeedType(), "Cache successfully cleared!");
        } else {
            Log.e(getFeedType(), "Cache not cleared successfully!");
        }

        posts.clear();
        fetchMorePosts();
    }

    /**
     * Returns a Post from specified index in the posts array
     * @param index - Index of Post in posts array
     * @return      - Post object
     */
    public Post getPost(int index) {
        Log.d(getFeedType(), "Cursor index is " + postCursor);
        return posts.get(index);
    }

    /**
     * Fetches more posts to populate the feed when it runs out of posts to display
     */
    public abstract void fetchMorePosts();

    /**
     * Adds a Post object to the static posts array
     * @param p - Post
     */
    public void addPost(Post p) {
        posts.add(p);
        this.setChanged();
        this.notifyObservers();
    }

    /**
     * Adds many posts at once.
     * @param p - ArrayList of Post objects
     */
    public void addPost(Vector<Post> p) {
        posts.addAll(p);

        this.setChanged();
        this.notifyObservers();
    }

    /**
     * Sets the latest ID in the PostHolder.  Should only be called by post fetch runnable
     * @param _latestID  - The latest post ID
     */
    protected void setLatestID(String _latestID) {
        latestID = _latestID;
    }

    /**
     * Retrieves a post index from the internal array by its URL.
     * @param url URL of the post to be retrieved.
     * @return The index of said post (0 if unsuccessful).
     */
    public int getPostPositionByUrl(String url) {
        for (int i = 0; i < posts.size(); i++) {
            if (posts.get(i).getUrl().equals(url))
                return i;
        }
        return 0;
    }

    /**
     * Retrieves a Post reference from the internal array by its URL.
     * @param url URL of the post to be retrieved.
     * @return Reference to said post (null if unsuccessful).
     */
    public Post getPostByUrl(String url) {
        for (Post p : posts) {
            if (p.getUrl().equals(url)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Returns the first Post from the posts array
     * @return      - Post object
     */
    public Post getPost() {
        return getPost(0);
    }

    /**
     * Returns the size of the posts array
     * @return  - Size of posts
     */
    public int getSize() {
        return posts.size();
    }

    /**
     * Update the PostManger's stored geolocation
     * @param newLatitude the new latitude coordinate
     * @param newLongitude the new longitude coordinate
     */
    public void updateGeolocation(double newLatitude, double newLongitude) {
        latitude = newLatitude;
        longitude = newLongitude;
    }
}
