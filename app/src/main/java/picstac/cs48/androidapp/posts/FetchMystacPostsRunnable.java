package picstac.cs48.androidapp.posts;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;
import picstac.cs48.androidapp.utils.MyStacUtility;

/**
 * Created by nick on 3/7/15.
 * Runnable that will fetch MyStac posts from the server.
 */
public class FetchMystacPostsRunnable implements Runnable {
    public interface FetchMystacPostsEvent {
        public void fetchMystacPostsEvent(Vector<Post> posts);
    }

    private Vector<Post> posts;
    private FetchMystacPostsEvent callback;
    private String queryBase = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.getid_api);

    public FetchMystacPostsRunnable(FetchMystacPostsEvent callback) {
        this.callback = callback;
        this.posts = new Vector<Post>();
    }

    @Override
    public void run() {
        try {
            if (MyStacUtility.getIDS().size() == 0) return;

            StringBuilder sBuilder = new StringBuilder(queryBase);
            for (Object o : MyStacUtility.getIDS()) {
                sBuilder.append("id=");
                sBuilder.append((String) o);
                sBuilder.append("&");
            }
            String query = sBuilder.toString();
            if (query.endsWith("&")) {
                query = query.substring(0, query.length()-1);
                Log.d("FetchMPR", "Cut trailing ampersand, new query is: " + query);
            }
            Log.d("FetchMPR", "Query string is: " + query);

            Log.d("ajksdhjkd", query);

            HttpURLConnection conn = (HttpURLConnection) new URL(query).openConnection();
            conn.setRequestProperty("Accept", "application/json");
            InputStream is = conn.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is), 1024);

            for (String line = r.readLine(); line != null; line = r.readLine()) {
                sb.append(line);
            }

            try {
                is.close();
            } catch (IOException e) {
                // There should be no issues closing the input stream
                Log.e("FetchMSPostsRunnable", e.toString());
            }

            String response = sb.toString();

            // Get array of post data
            JSONArray document = new JSONArray(response);

            // Iterate through and add data to a new post object
            for (int i = 0; i < document.length(); i++) {
                JSONObject p = document.getJSONObject(i);
                Post newPost = new Post(p.getString("url"), p.getInt("score"), p.getDouble("lat"),
                        p.getDouble("lon"), p.getString("_id"), p.getString("caption"), p.getInt("vote"));
                Log.d("FetchMSPostsRunnable", "Loaded new post with caption: " + newPost.getCaption() + " and ID: " + newPost.getId());

                this.posts.add(newPost);
            }

            try {
                callback.fetchMystacPostsEvent(this.posts);
            } catch (NullPointerException npe) {}
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
