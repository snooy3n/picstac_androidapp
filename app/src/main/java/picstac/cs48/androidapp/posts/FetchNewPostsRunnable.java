package picstac.cs48.androidapp.posts;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;

/**
 * Created by nick on 1/24/15.
 * Runnable that will fetch new posts from the server.
 */
public class FetchNewPostsRunnable implements Runnable {
    public interface PostsFetchedEvent {
        public void postsFetchedEvent(Vector<Post> posts);
    }

    static final String getNewestUrl = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.new_api);
    static final String getNewestUrlWithId = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.page_api);

    private String latestID;
    private double latitude = 0.0;
    private double longitude = 0.0;
    protected PostsFetchedEvent callback;
    protected Vector<Post> posts;

    public FetchNewPostsRunnable(PostsFetchedEvent callback, String latestID, double latitude, double longitude) {
        this.callback = callback;
        this.latestID = latestID;
        this.latitude = latitude;
        this.longitude = longitude;
        this.posts = new Vector<Post>(PostManager.POST_FETCH_BATCH_SIZE);
    }

    public FetchNewPostsRunnable() {}

    /**
     * Retrieves posts from the server.
     * @param query HTTP query string used to contact the server.
     */
    protected void requestPostsFromServer(String query) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(query).openConnection();
            conn.setRequestProperty("Accept", "application/json");
            InputStream is = conn.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is), 1024);

            for (String line = r.readLine(); line != null; line = r.readLine()) {
                sb.append(line);
            }

            try {
                is.close();
            } catch (IOException e) {
                // There should be no issues closing the input stream
                Log.e("FetchNewPostsRunnable", e.toString());
            }

            String response = sb.toString();

            // Get array of post data
            JSONArray document = new JSONArray(response);

            // Iterate through and add data to a new post object
            for (int i = 0; i < document.length(); i++) {
                JSONObject p = document.getJSONObject(i);
                Post newPost = new Post(p.getString("url"), p.getInt("score"), p.getDouble("lat"),
                        p.getDouble("lon"), p.getString("_id"), p.getString("caption"), p.getInt("vote"));
                Log.d("FetchNewPostsRunnable", "Loaded new post with caption: " + newPost.getCaption() + " and ID: " + newPost.getId());

                this.posts.add(newPost);
            }

            callback.postsFetchedEvent(this.posts);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        // Set current thread priority to background
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        String query;

        if (latestID.equals("")) {
            // Construct our query
            query = getNewestUrl + "lat=" + latitude + "&lon=" + longitude;
            Log.e("FetchNewPostsRunnable", "Querying without ID");
        } else {
            Log.d("FetchNewPostsRunnable", "Querying with ID " + this.latestID);
            query = getNewestUrlWithId + "id=" + latestID + "&lat=" + latitude + "&lon=" + longitude;
        }

        requestPostsFromServer(query);
    }
}
