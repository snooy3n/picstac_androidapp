package picstac.cs48.androidapp.posts;

import android.util.Log;

import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * Created by nick on 3/7/15.
 * Postmanager to handle the MyStac feed.
 */
public class MystacManager extends PostManager implements
        FetchMystacPostsRunnable.FetchMystacPostsEvent {

    @Override
    public String getFeedType() {
        return "mystac";
    }

    /**
     * Fetches more posts from the server.
     */
    @Override
    public void fetchMorePosts() {
        Log.d("MystacManager", "fetchMorePosts called with coords (" + latitude + "," + longitude + ")");
        if (!threadRunning) {
            threadRunning = true;
            postFetchThreadRef = new Thread(new FetchMystacPostsRunnable(this));
            postFetchThreadRef.start();
        }
    }

    /**
     * Callback called by FetchMystacPosts when it fetches new posts
     * @param newPosts  - A Vector of the new posts the runnable fetched from the server
     */
    @Override
    public void fetchMystacPostsEvent(Vector<Post> newPosts) {
        try {
            Log.d("MystacManager", "Posts were fetched!  Latest ID is " + newPosts.lastElement().getId());
            this.addPost(newPosts);
            threadRunning = false;
            retries = 0;
        } catch (NoSuchElementException e) {
            Log.e("MystacManager", "PostsFetchedEvent contained an empty array!");

            // Try again
            threadRunning = false;
            retries++;

            if (retries > 10) {
                return;
            }

            fetchMorePosts();
        }
    }
}
