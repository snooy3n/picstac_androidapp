package picstac.cs48.androidapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import picstac.cs48.androidapp.utils.BitmapFileUtility;
import picstac.cs48.androidapp.utils.ServerUploadThread;

/**
 * Activity that handles user image upload.
 */
public class DisplayImageActivity extends ActionBarActivity implements ServerUploadThread.ServerUploadThreadCallback, View.OnClickListener {

    private static final int REQ_WIDTH = 720;
    private static final int REQ_HEIGHT = 720;
    private static final int IMG_WIDTH = 720;
    private static final int IMG_HEIGHT = 720;

    private ImageView       imgView;
    private String          imageAbsPath;
    private Bitmap          bitmapToBeUploaded;
    private ProgressDialog  progressDialog;
    private Button          uploadButton;
    private Context         context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_image);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bitmapToBeUploaded = null;
        imgView = (ImageView) findViewById(R.id.img_preview);
        uploadButton = (Button) findViewById(R.id.upload_button);
        uploadButton.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);

        context = this;

        setViewToImage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_display_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);

    }

    @Override
    protected void onStop() {
        if (bitmapToBeUploaded != null) {
            bitmapToBeUploaded.recycle();
            bitmapToBeUploaded = null;
        }

        super.onStop();
    }

    /**
     * Callback for when the server request finishes.
     */
    @Override
    public void serverPostThreadFinished(boolean timedOut) {
        progressDialog.dismiss();
        if(timedOut) {

            this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(context, "Upload failed! Try again with better connection.", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upload_button:
                onClickUploadButton(v);
                break;
            default:
                throw new RuntimeException("View ID doesn't match!");
        }
    }

    /**
     * Code to handle when the upload button is pressed.
     * @param view the view that was pressed.
     */
    public void onClickUploadButton(View view) {
        if(bitmapToBeUploaded == null) {
            return;
        }
        if(progressDialog != null) {
            progressDialog.setTitle("Uploading image");
            progressDialog.setMessage("Please wait.");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }
        EditText captionText = (EditText) findViewById(R.id.edit_caption_field);
        String caption = scrubCaption(captionText.getText().toString());
        if(caption == null) {
            caption = "";
        }

        String latitude = String.valueOf(Picstac.getInstance().getLocation().getLatitude());
        String longitude = String.valueOf(Picstac.getInstance().getLocation().getLongitude());
   
        ServerUploadThread t = new ServerUploadThread(bitmapToBeUploaded, caption, this, latitude, longitude, Picstac.android_id);
        t.start();
    }

    /**
     * Scrubs the caption input of illegal characters
     * @param caption the caption
     * @return the scrubbed caption
     */
    private String scrubCaption(String caption) {
        if(caption == null) {
            return null;
        }
        return caption.replaceAll("[^a-zA-Z0-9;\\-_\\/().,~+=?: ]", "");
    }

    /**
     * Sets the preview image view to the captured photo.
     */
    private void setViewToImage() {
        Bundle extras = getIntent().getExtras();
        int rotationFactor = 0;
        if(extras != null) {
            imageAbsPath = extras.getString(CameraActivity.FILE_PATH);
            rotationFactor = extras.getInt(CameraActivity.ORIENTATION);
        }

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageAbsPath, bmOptions);
        bmOptions.inSampleSize = BitmapFileUtility.calculateInSampleSize(bmOptions, REQ_WIDTH, REQ_HEIGHT);

        bmOptions.inJustDecodeBounds = false;
        Bitmap origBitmap = BitmapFactory.decodeFile(imageAbsPath, bmOptions);

        if(origBitmap != null) {
            origBitmap = BitmapFileUtility.rotateBitmap(origBitmap, rotationFactor);
            origBitmap = BitmapFileUtility.cropBitmapToSquare(origBitmap);
            origBitmap = Bitmap.createScaledBitmap(origBitmap, IMG_WIDTH, IMG_HEIGHT, true);

            imgView.setImageBitmap(origBitmap);
            bitmapToBeUploaded = origBitmap;
        }
        if(bitmapToBeUploaded == null) {
            this.finish();
        }
    }
}
