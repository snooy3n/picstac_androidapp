package picstac.cs48.androidapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import picstac.cs48.androidapp.feed.FeedFragment;
import picstac.cs48.androidapp.stacs.MyUploadsFragment;
import picstac.cs48.androidapp.stacs.MystacFragment;

/**
 * Created by nick on 2/7/15.
 * Adapter that handles the tabbed view in the MainActivity.
 */
public class MainActivityPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT;
    private CharSequence tabTitles[];

    public MainActivityPagerAdapter(FragmentManager fm, CharSequence[] titles, int numbtabs) {
        super(fm);
        PAGE_COUNT = numbtabs;
        tabTitles = titles;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FeedFragment.newInstance("new");
            case 1:
                return FeedFragment.newInstance("popular");
            case 2:
                return MystacFragment.newInstance();
            case 3:
                return MyUploadsFragment.newInstance();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
