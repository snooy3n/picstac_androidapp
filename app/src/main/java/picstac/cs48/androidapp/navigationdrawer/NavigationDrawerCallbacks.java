package picstac.cs48.androidapp.navigationdrawer;

/**
 * Interface must be implemented by all activities that contain a NavigationDrawerFragment
 * Created by poliveira on 27/10/2014.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
