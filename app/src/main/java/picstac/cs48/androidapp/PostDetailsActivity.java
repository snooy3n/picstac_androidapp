package picstac.cs48.androidapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import picstac.cs48.androidapp.posts.MystacManager;
import picstac.cs48.androidapp.posts.Post;
import picstac.cs48.androidapp.posts.PostManager;
import picstac.cs48.androidapp.utils.MyStacUtility;
import picstac.cs48.androidapp.utils.NetworkCacheableImageView;

/**
 * Created by Nick on 2/27/2015.
 * Activity that shows detailed information for a specific Post.
 */
public class PostDetailsActivity extends Activity implements OnMapReadyCallback, View.OnClickListener {
    private Post mPost;
    private String mUrl;
    private PostManager mPm;

    private MapView mapView;
    private LatLng currLatLng;
    private Button mystacButton;
    private NetworkCacheableImageView imageView;
    private TextView caption;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_image_details);

        Intent thisIntent = getIntent();
        Bundle args = thisIntent.getExtras();
        this.mUrl = args.getString("URL");
        this.mPm = Picstac.getInstance().getPostManager(args.getString("feedType"));
        this.mPost = mPm.getPostByUrl(mUrl);
        this.currLatLng = new LatLng(mPost.getLatitude(), mPost.getLongitude());

        mapView = (MapView) findViewById(R.id.mapview);
        mystacButton = (Button) findViewById(R.id.mystacButton);
        mystacButton.setOnClickListener(this);

        if (this.mPm instanceof MystacManager) {
            mystacButton.setEnabled(false);
        }

        caption = (TextView) findViewById(R.id.feedDetailsCaption);
        imageView = (NetworkCacheableImageView) findViewById(R.id.feed_details_imageview);
        caption.setText(mPost.getCaption());

        imageView.loadImage(mUrl, true, null);

        MapsInitializer.initialize(this);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * Called when a google map is ready for use.
     * @param googleMap A non-null GoogleMap ready for use.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("PostDetailsActivity", "Current coords: " + currLatLng.toString());
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.currLatLng, 12));
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
            }
        });
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        MyStacUtility.addID(this.mPost.getId());
        Log.d("PostDetailsActivity", MyStacUtility.printAll());
        Toast.makeText(v.getContext(), "Added image to MyStac!", Toast.LENGTH_SHORT).show();
    }
}
