package picstac.cs48.androidapp.feed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.PostDetailsActivity;
import picstac.cs48.androidapp.R;
import picstac.cs48.androidapp.posts.Post;
import picstac.cs48.androidapp.posts.PostManager;
import picstac.cs48.androidapp.utils.NetworkCacheableImageView;
import picstac.cs48.androidapp.utils.SendVoteRunnable;

/**
 * Created by nick on 1/29/15.
 * This class represents each CardView that will be displayed in the feed.
 */
public class FeedPostViewHolder extends RecyclerView.ViewHolder implements SendVoteRunnable.SendVoteFinished, View.OnClickListener {

    public TextView vCaption, votesView;
    public NetworkCacheableImageView imgView;
    public ImageButton downVoteButton, upVoteButton;
    public String url;
    public PostManager pm;

    private Context context;
    private View thisView;

    /**
     * Handler to interface between the Vote request callback and the UI thread.
     */
    static class IncomingHandler extends Handler {
        private final WeakReference<FeedPostViewHolder> mParent;

        IncomingHandler(Looper looper, FeedPostViewHolder feedPostViewHolder) {
            super(looper);
            mParent = new WeakReference<FeedPostViewHolder>(feedPostViewHolder);
        }

        @Override
        public void handleMessage(Message msg) {
            FeedPostViewHolder _parent = mParent.get();
            if (_parent != null) {
                Log.e("FeedPostViewHolder", "Handler called!");

                Bundle b = msg.getData();
                int newVotes = b.getInt("newVotes");

                Post p = _parent.pm.getPostByUrl(_parent.url);

                if (newVotes == p.getVotes()) {
                    Log.e("FeedPostViewHolder", "User has already voted!");
                    Toast.makeText(_parent.context, "You have already voted on this pic!", Toast.LENGTH_SHORT).show();
                    return;
                } else if (newVotes > p.getVotes()) {
                    Log.d("FeedPostViewHolder", "User has successfully upvoted!");
                    Toast.makeText(_parent.context, "You have upvoted this pic!", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("FeedPostViewHolder", "User has successfully downvoted!");
                    Toast.makeText(_parent.context, "You have downvoted this pic!", Toast.LENGTH_SHORT).show();
                }
                p.setVotes(newVotes);

                _parent.votesView.setText(Integer.toString(p.getVotes()));
            } else {
                Log.e("FeedPostViewHolder", "NullPointerException caught in handler!");
            }
        }
    }

    public FeedPostViewHolder(View v){
        super(v);
        thisView = v;
        context = v.getContext();
        vCaption = (TextView) v.findViewById(R.id.card_caption);
        imgView = (NetworkCacheableImageView) v.findViewById(R.id.card_imageview);
        downVoteButton = (ImageButton) v.findViewById(R.id.dvotebutton);
        upVoteButton = (ImageButton) v.findViewById(R.id.uvotebutton);
        votesView = (TextView) v.findViewById(R.id.votesView);

        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FeedPostViewHolder", "Card holding URL: " + url + " was clicked!");
                Intent intent = new Intent(v.getContext(), PostDetailsActivity.class);
                intent.putExtra("URL", url);
                intent.putExtra("feedType", pm.getFeedType());
                v.getContext().startActivity(intent);
            }
        });

        upVoteButton.setOnClickListener(this);
        downVoteButton.setOnClickListener(this);
    }

    /**
     * Callback for when SendVoteRunnable receives a response from the server.
     * @param newVotes 1 if successful, 0 if unsuccessful.
     * @param voteValue The value of the vote sent to the server.
     */
    @Override
    public void sendVoteFinished(boolean successful, int newVotes, int voteValue) {
        Log.d("FeedPostViewHolder", "sendVoteFinished callback triggered!");
        if (successful) {
            pm.getPostByUrl(url).setUserHasVoted(voteValue);
            IncomingHandler handler = new IncomingHandler(Looper.getMainLooper(), this);
            Message msg = handler.obtainMessage();

            Bundle b = new Bundle();
            b.putInt("newVotes", newVotes);
            b.putInt("voteValue", voteValue);
            msg.setData(b);

            handler.sendMessage(msg);
        }
    }

    /**
     * OnClick callback for the up/down vote buttons.
     * @param v The view calling the callback.
     */
    @Override
    public void onClick(View v) {
        Post p = pm.getPostByUrl(url);

        if (v == upVoteButton) {
            Log.d("FeedPostViewHolder", "Upvote button clicked for URL: " + url);
            new Thread(new SendVoteRunnable(Post.VOTE_UP, p.getId(), Picstac.android_id, this)).start();

        } else if (v == downVoteButton) {
            Log.d("FeedPostViewHolder", "Downvote button clicked for URL: " + url);
            new Thread(new SendVoteRunnable(Post.VOTE_DOWN, p.getId(), Picstac.android_id, this)).start();
        }
    }
}
