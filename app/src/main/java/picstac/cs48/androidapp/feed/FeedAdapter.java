package picstac.cs48.androidapp.feed;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import picstac.cs48.androidapp.R;
import picstac.cs48.androidapp.posts.Post;
import picstac.cs48.androidapp.posts.PostManager;

/**
 * Adapter that interfaces between various PostManagers and the UI.
 */
class FeedAdapter extends RecyclerView.Adapter<FeedPostViewHolder> {

    private PostManager pm;
    private static int viewCount = 0;

    /**
     * Called when a view created by this adapter has been recycled.
     * <p/>
     * <p>A view is recycled when a {@link android.support.v7.widget.RecyclerView.LayoutManager} decides that it no longer
     * needs to be attached to its parent {@link android.support.v7.widget.RecyclerView}. This can be because it has
     * fallen out of visibility or a set of cached views represented by views still
     * attached to the parent RecyclerView. If an item view has large or expensive data
     * bound to it such as large bitmaps, this may be a good place to release those
     * resources.</p>
     *
     * @param holder The ViewHolder for the view being recycled
     */
    @Override
    public void onViewRecycled(FeedPostViewHolder holder) {
        super.onViewRecycled(holder);
    }

    /**
     * Called when a view created by this adapter has been attached to a window.
     * <p/>
     * <p>This can be used as a reasonable signal that the view is about to be seen
     * by the user. If the adapter previously freed any resources in
     * {@link #onViewDetachedFromWindow(android.support.v7.widget.RecyclerView.ViewHolder) onViewDetachedFromWindow}
     * those resources should be restored here.</p>
     *
     * @param holder Holder of the view being attached
     */
    @Override
    public void onViewAttachedToWindow(FeedPostViewHolder holder) {
        holder.imgView.loadImage(holder.url, true, null);
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public int getItemCount(){
        return pm.getSize();
    }

    /**
     * FeedAdapter Constructor
     * @param pm Reference to the PostManager which this feed will display
     */
    public FeedAdapter(PostManager pm) {
        this.pm = pm;
    }

    /**
     * Called when a view created by this adapter has been detached from its window.
     * <p/>
     * <p>Becoming detached from the window is not necessarily a permanent condition;
     * the consumer of an Adapter's views may choose to cache views offscreen while they
     * are not visible, attaching an detaching them as appropriate.</p>
     *
     * @param holder Holder of the view being detached
     */
    @Override
    public void onViewDetachedFromWindow(FeedPostViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    /**
     * Called when a new feedpostviewholder is added to the adapter
     * @param feedPostViewHolder The newly created feedpostviewholder
     * @param i The position of above viewholder
     */
    @Override
    public void onBindViewHolder(FeedPostViewHolder feedPostViewHolder, int i){
        try {
            Post feedPost = pm.getPost(i);
            feedPostViewHolder.pm = pm;
            feedPostViewHolder.vCaption.setText(feedPost.getCaption());
            feedPostViewHolder.url = feedPost.getUrl();
            feedPostViewHolder.votesView.setText(Integer.toString(feedPost.getVotes()));

        } catch (IndexOutOfBoundsException e) {
            Log.e("FeedAdapter", "Index out of bounds exception encountered!");
        }
    }

    /**
     * Called when a new feedpostviewholder is created.
     * @param viewGroup The parent view that the viewholder will be put into.
     * @param i The position of above viewholder.
     * @return The new FeedPostViewHolder
     */
    @Override
    public FeedPostViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_cardview, viewGroup, false);
        Log.e("FeedAdapter", "Viewcount: " + ++viewCount);
        return new FeedPostViewHolder(itemView);
    }
}
