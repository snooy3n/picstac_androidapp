package picstac.cs48.androidapp.feed;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.Observable;
import java.util.Observer;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;
import picstac.cs48.androidapp.posts.PostManager;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FeedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FeedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedFragment extends android.support.v4.app.Fragment implements Observer, SwipeRefreshLayout.OnRefreshListener {

    private String feedType = "new";
    private static final String FEED_TYPE_KEY = "feedType";

    private FeedAdapter feedAdapter;
    private SwipeRefreshLayout swipeLayout;
    private PostManager pm;

    private OnFragmentInteractionListener mCallback;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment FeedFragment.
     */
    public static FeedFragment newInstance(String _feedType) {
        FeedFragment fragment = new FeedFragment();

        Bundle args = new Bundle();
        args.putString(FEED_TYPE_KEY, _feedType);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Empty fragment default constructor
     */
    public FeedFragment() {
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            feedType = args.getString(FEED_TYPE_KEY, "new");
        }

        this.pm = Picstac.getInstance().getPostManager(feedType);
        feedAdapter = new FeedAdapter(pm);
        pm.addObserver(this);
        pm.fetchMorePosts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.feed_fragment_layout, container, false);
        final RecyclerView imageList = (RecyclerView) view.findViewById(R.id.ImageFeedList);
        final LinearLayoutManager llm = new LinearLayoutManager(container.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        imageList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean hideToolBar = false;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                mCallback.hideToolBarOnScroll(hideToolBar);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 20) {
                    hideToolBar = true;

                } else if (dy < -5) {
                    hideToolBar = false;
                }
            }

        });

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorDarkPrimary);

        imageList.setHasFixedSize(true);
        imageList.setLayoutManager(llm);
        imageList.setAdapter(feedAdapter);

        Log.d("FeedFragment", "onCreateView called!");
        return view;
    }

    /**
     * Callback for when the users pulls down to refresh
     */
    @Override
    public void onRefresh() {
        Picstac.getInstance().reloadAllFeeds();
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                swipeLayout.setRefreshing(false);
            }
        }, 2000);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void hideToolBarOnScroll(boolean hideToolBar);  //Hide or show tool bar on scroll down
    }

    /**
     * Handler that interfaces between the update callback and the UI thread.
     */
    static class IncomingHandler extends Handler {
        private final WeakReference<FeedAdapter> mAdapter;

        IncomingHandler(Looper looper, FeedAdapter mAdapter) {
            super(looper);
            this.mAdapter = new WeakReference<FeedAdapter>(mAdapter);
        }

        @Override
        public void handleMessage(Message msg) {
            FeedAdapter _adapter = mAdapter.get();
            if (_adapter != null) {
                _adapter.notifyDataSetChanged();
            } else {
                Log.e("FeedFragment", "NullPointerException caught in handler!");
            }
        }
    }

    /**
     * Callback that the PostManager associated with this Feed calls when it updates its
     * data set.
     * @param observable The PostManager object reference.
     * @param data Null in this case.
     */
    @Override
    public void update(Observable observable, Object data) {
        Log.e("FeedFragment", "update called!");
        IncomingHandler handler = new IncomingHandler(Looper.getMainLooper(), this.feedAdapter);
        Message msg = handler.obtainMessage(1);
        handler.sendMessage(msg);
    }
}