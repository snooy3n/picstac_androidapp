package picstac.cs48.androidapp.stacs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;

/**
 * Created by Nick on 2/26/2015.
 * Fragment that handles the Uploads feed.
 */
public class MyUploadsFragment extends StacFragment {

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment MystacFragment.
     */
    public static MyUploadsFragment newInstance() {
        return new MyUploadsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pm = Picstac.getInstance().getPostManager("uploads");
        pm.addObserver(this);
        pm.fetchMorePosts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("MyUploadsFragment", "View created!");
        thisView = inflater.inflate(R.layout.mystac_fragment_layout, container, false);
        mGridView = (GridView) thisView.findViewById(R.id.gridView);

        mAdapter = new StacAdapter(this.getActivity(), pm);
        mGridView.setAdapter(mAdapter);

        swipeLayout = (SwipeRefreshLayout) thisView.findViewById(R.id.swipe_container_mystacfeed);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorDarkPrimary);

        return thisView;
    }
}
