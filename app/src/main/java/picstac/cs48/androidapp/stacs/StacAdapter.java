package picstac.cs48.androidapp.stacs;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import picstac.cs48.androidapp.PostDetailsActivity;
import picstac.cs48.androidapp.R;
import picstac.cs48.androidapp.posts.Post;
import picstac.cs48.androidapp.posts.PostManager;
import picstac.cs48.androidapp.utils.NetworkCacheableImageView;

/**
 * Created by nick on 3/22/15.
 */
public class StacAdapter extends BaseAdapter {
    private Context mContext;
    private PostManager pm;

    public StacAdapter(Context mContext, PostManager pm) {
        this.mContext = mContext;
        this.pm = pm;
    }

    @Override
    public int getCount() {
        return pm.getSize();
    }

    @Override
    public Post getItem(int position) {
        return pm.getPost(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.mystac_item_layout, parent, false);
        }

        NetworkCacheableImageView imageView = (NetworkCacheableImageView) convertView
                .findViewById(R.id.nciv_mystac);

        imageView.loadImage(pm.getPost(position).getUrl(), false, null);
        imageView.setOnClickListener(new ButtonListener(pm.getPost(position), pm.getFeedType()));

        return convertView;
    }

    class ButtonListener implements View.OnClickListener {
        private Post p;
        private String feedType;

        ButtonListener(Post p, String feedType) {
            this.p = p;
            this.feedType = feedType;
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override

        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), PostDetailsActivity.class);
            intent.putExtra("URL", p.getUrl());
            intent.putExtra("feedType", feedType);
            v.getContext().startActivity(intent);
        }
    }
}
