package picstac.cs48.androidapp.stacs;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import java.lang.ref.WeakReference;
import java.util.Observable;
import java.util.Observer;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.posts.PostManager;

/**
 * Created by nick on 3/22/15.
 */
public abstract class StacFragment extends android.support.v4.app.Fragment
        implements Observer, SwipeRefreshLayout.OnRefreshListener {
    protected View thisView;
    protected GridView mGridView;
    protected StacAdapter mAdapter;
    protected PostManager pm;
    protected SwipeRefreshLayout swipeLayout;

    /**
     * Handler that interfaces between the update callback and the UI thread.
     */
    static class IncomingHandler extends Handler {
        private final WeakReference<StacAdapter> mAdapter;

        IncomingHandler(Looper looper, StacAdapter mAdapter) {
            super(looper);
            this.mAdapter = new WeakReference<StacAdapter>(mAdapter);
        }

        @Override
        public void handleMessage(Message msg) {
            StacAdapter _adapter = mAdapter.get();
            if (_adapter != null) {
                _adapter.notifyDataSetChanged();
            } else {
                Log.e("StacFragment", "NullPointerException caught in handler!");
            }
        }
    }

    /**
     * Callback that the PostManager associated with this Feed calls when it updates its
     * data set.
     * @param observable The PostManager object reference.
     * @param data Null in this case.
     */
    @Override
    public void update(Observable observable, Object data) {
        Log.e("StacFragment", "update called!");
        IncomingHandler handler = new IncomingHandler(Looper.getMainLooper(), this.mAdapter);
        handler.sendMessage(handler.obtainMessage(1));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void hideToolBarOnScroll(boolean hideToolBar);  //Hide or show tool bar on scroll down
    }

    /**
     * Callback for when the users pulls down to refresh
     */
    @Override
    public void onRefresh() {
        Picstac.getInstance().reloadAllFeeds();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
            }
        }, 2000);
    }
}
