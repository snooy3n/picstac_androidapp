package picstac.cs48.androidapp;

import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import picstac.cs48.androidapp.utils.CameraCallbacks;
import picstac.cs48.androidapp.utils.CameraPreview;

/**
 * Class that handles the Camera implementation, interfacing between the hardware camera
 * and the PicStac application.
 */
public class CameraActivity extends ActionBarActivity implements View.OnClickListener {

    public static final String FILE_PATH = "com.cs48.picstac.path";
    public static final String ORIENTATION = "com.cs48.picstac.photoOrientation";

    private Camera camera;
    private CameraPreview cameraPreview;
    private ImageButton swapButton;
    private ImageButton captureButton;
    private FrameLayout layoutBackground;

    /**
     * Callback for camera autofocus feature
     */
    private Camera.AutoFocusCallback autoFocusCallback = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean flag, Camera camera) {
            captureButton.setEnabled(true);
            swapButton.setEnabled(true);
        }
    };

    private int photoOrientation = 0;
    private int frontCameraID = -1;
    private int rearCameraID = -1;
    private int currentCamera = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        swapButton = (ImageButton) findViewById(R.id.button_swap);
        captureButton = (ImageButton) findViewById(R.id.button_capture);
        layoutBackground = (FrameLayout) findViewById(R.id.camera_preview);

        captureButton.setOnClickListener(this);
        swapButton.setOnClickListener(this);
        layoutBackground.setOnClickListener(this);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            Toast.makeText(this, "No camera found!", Toast.LENGTH_LONG).show();
            this.finish();
        }

        findBothCameraIDs();
        initializeCamera(currentCamera);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        if (camera != null) {
            camera.release();
            camera = null;
        }
        super.onPause();
        finish();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_swap:
                initializeCamera(getNextCameraAndSwap());
                break;
            case R.id.button_capture:
                camera.takePicture(null, null, new CameraCallbacks(this, photoOrientation));
                break;
            case R.id.camera_preview:
                captureButton.setEnabled(false);
                swapButton.setEnabled(false);
                camera.autoFocus(autoFocusCallback);
                break;
            default:
                throw new RuntimeException("View ID doesn't match buttons!");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Initializes the camera.
     * @param cameraID the ID of the camera to be initialized (either front or back)
     */
    private void initializeCamera(int cameraID) {
        camera = getCameraInstance(cameraID);
        if(camera != null) {
            camera.setDisplayOrientation(90);
        }
        if(cameraID == rearCameraID) {
            photoOrientation = 90;
        } else if(cameraID == frontCameraID) {
            photoOrientation = 270;
        }
        cameraPreview = new CameraPreview(this, camera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.removeAllViews();
        preview.addView(cameraPreview);
        preview.addView(swapButton);
        preview.addView(captureButton);
    }

    /**
     * Returns an available camera instance
     * @param cameraID the camera to open
     * @return a camera instance
     */
    private Camera getCameraInstance(int cameraID){
        if(camera != null) {
            camera.release();
            camera = null;
        }

        try {
            camera = Camera.open(cameraID);
            Log.d("Camera", "Opening camera with ID " + cameraID);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if(camera == null) {
            Toast.makeText(this, "Failed to open camera!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        return camera;
    }

    /**
     * Helper funtion to swap cameras, if possible.
     * @return the ID of the next camera
     */
    private int getNextCameraAndSwap() {
        // happens when there is no front camera
        if(frontCameraID == -1) {
            return rearCameraID;
        }

        // otherwise perform logic of swapping cameras
        if(currentCamera == rearCameraID) {
            currentCamera = frontCameraID;
            return frontCameraID;
        } else if(currentCamera == frontCameraID) {
            currentCamera = rearCameraID;
            return rearCameraID;
        } else {
            return -1;
        }
    }

    /**
     * A helper function that finds and returns the IDs of the cameras available on the user's device
     */
    private void findBothCameraIDs() {
        int numCameras = Camera.getNumberOfCameras();
        for(int i = 0; i < numCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                frontCameraID = i;
            } else if(info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                rearCameraID = i;
            }
        }
        currentCamera = rearCameraID;
        Log.d("Camera", "f: " + frontCameraID + " r: " + rearCameraID);
    }
}
