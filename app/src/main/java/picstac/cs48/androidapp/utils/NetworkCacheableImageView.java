package picstac.cs48.androidapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.RejectedExecutionException;

import picstac.cs48.androidapp.Picstac;
import uk.co.senab.bitmapcache.BitmapLruCache;
import uk.co.senab.bitmapcache.CacheableBitmapDrawable;
import uk.co.senab.bitmapcache.CacheableImageView;

/**
 * Created by nick on 1/24/15.
 * This class borrows heavily from Chris Banes' sample implementation of his cache
 * that is also used in this application.  The original source code can be found
 * here: https://github.com/chrisbanes/Android-BitmapCache/blob/master/sample/src/uk/co/senab/bitmapcache/samples/NetworkedCacheableImageView.java
 */
public class NetworkCacheableImageView extends CacheableImageView {
    public interface OnImageLoadedListener {
        void onImageLoaded(CacheableBitmapDrawable result);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d("NCImageView", "Height: " + this.getHeight());
    }

    public static final int FADE_IN_TIME = 500;
    private static final int DEFAULT_WIDTH = 640;
    private static final int DEFAULT_HEIGHT = 640;
    private static Bitmap placeholderImage = Bitmap.createBitmap(DEFAULT_WIDTH, DEFAULT_HEIGHT, Bitmap.Config.ARGB_4444);

    /**
     * Async task to fetch bitmap from URL.
     */
    private static class DownloadImageAsyncTask extends
            AsyncTask<String, Void, CacheableBitmapDrawable> {

        private final BitmapLruCache mCache;
        private final WeakReference<ImageView> mImageViewRef;
        private final OnImageLoadedListener mListener;
        private final BitmapFactory.Options mDecodeOpts;

        DownloadImageAsyncTask(ImageView imageView, BitmapLruCache cache,
                               BitmapFactory.Options decodeOpts, OnImageLoadedListener listener) {

            mCache = cache;
            mImageViewRef = new WeakReference<ImageView>(imageView);
            mListener = listener;
            mDecodeOpts = decodeOpts;
        }

        /**
         * Downloads image specified by URL
         *
         * @param params The URL of the image to be downloaded.
         * @return A result, defined by the subclass of this task.
         */
        @Override
        protected CacheableBitmapDrawable doInBackground(String... params) {
            try {
                // Return immediately if the imageView reference has already been released
                if (mImageViewRef.get() == null)
                    return null;

                final String url = params[0];
                CacheableBitmapDrawable result;

                try {
                    result = mCache.get(url, mDecodeOpts);
                } catch (OutOfMemoryError outOfMemoryError) {
                    Log.e("NCImageView", "Ran out of memory!");
                    outOfMemoryError.printStackTrace();
                    return null;
                }

                if (result == null) {
                    Log.d("DownloadImageAsyncTask", "Downloading " + url);

                    // At this point we know that the bitmap isn't cached, so download it
                    // from the web.
                    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                    InputStream is = new BufferedInputStream(conn.getInputStream());

                    // Add to cache
                    try {
                        result = mCache.put(url, is, mDecodeOpts);
                    } catch (OutOfMemoryError outOfMemoryError) {
                        outOfMemoryError.printStackTrace();
                    }

                    is.close();
                    conn.disconnect();
                } else {
                    Log.d("DownloadImageAsyncTask", "Got from cache: " + url);
                }

                return result;

            } catch (IOException e) {
                Log.e("DownloadImageAsyncTask", e.toString());
                Log.e("DownloadImageAsyncTask", "Trying again...");

            }

            return null;
        }

        /**
         * <p>Runs on the UI thread after {@link #doInBackground}. The
         * specified result is the value returned by {@link #doInBackground}.</p>
         * <p/>
         * <p>This method won't be invoked if the task was cancelled.</p>
         *
         * @param result The result of the operation computed by {@link #doInBackground}.
         */
        @Override
        protected void onPostExecute(final CacheableBitmapDrawable result) {
            super.onPostExecute(result);

            ImageView iv = mImageViewRef.get();
            if (iv != null) {
                iv.setImageDrawable(result);

                Animation fadein = new AlphaAnimation(0, 1);
                fadein.setInterpolator(new AccelerateInterpolator());
                fadein.setStartOffset(0);
                fadein.setDuration(FADE_IN_TIME);
                iv.setAnimation(fadein);

            } else {
                Log.e("NCImageView", "ImageView is null!");
            }

            if (mListener != null) {
                mListener.onImageLoaded(result);
            }
        }
    }


    private final BitmapLruCache mCache;
    private DownloadImageAsyncTask mCurrentTask;

    public NetworkCacheableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mCache = Picstac.getApplication(context).getBitmapCache();
    }

    /**
     * Loads the bitmap
     * @param url       - URL of the image to be downloaded
     * @param fullSize  - Whether the image should be kept at original size
     * @param listener  - Callback for image being loaded
     * @return          - True if the bitmap was found in the cache (either mem or disk)
     */
    public boolean loadImage(String url, final boolean fullSize, OnImageLoadedListener listener) {
        // Cancel any already-running tasks
        if (mCurrentTask != null)
            mCurrentTask.cancel(true);

        // Check to see if mem cache has bitmap
        // This is safe to do on UI thread
        BitmapDrawable wrapper = mCache.getFromMemoryCache(url);

        if (wrapper != null) {
            // It's in the mem cache, so just display it
            setImageDrawable(wrapper);

            return true;
        } else {
            // Mem cache doesn't have URL, do threaded req
//            setImageDrawable(null);
            setImageBitmap(placeholderImage);
            BitmapFactory.Options decodeOpts = null;

            mCurrentTask = new DownloadImageAsyncTask(this, mCache, decodeOpts, listener);

            try {
                //mCurrentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                mCurrentTask.execute(url);
            } catch (RejectedExecutionException e) {
                // Shouldn't happen
            }
            return false;
        }
    }
}
