package picstac.cs48.androidapp.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;
import picstac.cs48.androidapp.posts.Post;

/**
 * Created by nick on 2/7/15.
 * Runnable that sends and retrieves vote stats regarding specific posts between the
 * app and the server.
 */
public class SendVoteRunnable implements Runnable {
    public interface SendVoteFinished {
        public void sendVoteFinished(boolean successful, int newVotes, int voteValue);
    }

    private final String voteBaseUrl = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.vote_api);
    private int vote;
    private String postID, userID;
    private SendVoteFinished callback;

    public SendVoteRunnable(int vote, String postID, String userID, SendVoteFinished callback) {
        this.vote = vote;
        this.postID = postID;
        this.userID = userID;
        this.callback = callback;
    }

    @Override
    public void run() {
        String responseString = "";
        int _vote = 0;
        switch (vote) {
            case Post.VOTE_UP:
                _vote = 1;
                break;
            case Post.VOTE_DOWN:
                _vote = -1;
                break;
        }

        try {
            String voteString = Integer.toString(_vote);
            String voteUrl = voteBaseUrl + "id=" + postID + "&user=" + userID + "&vote=" + voteString;
            Log.d("SendVoteRunnable", "Sending vote with URL: " + voteUrl);

            HttpURLConnection conn = (HttpURLConnection) new URL(voteUrl).openConnection();
            conn.setRequestProperty("Accept", "application/json");
            InputStream is = conn.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is), 1024);

            for (String line = r.readLine(); line != null; line = r.readLine()) {
                sb.append(line);
            }

            try {
                is.close();
            } catch (IOException e) {
                // There should be no issues closing the input stream
                Log.e("FetchNewPostsRunnable", e.toString());
            }

            responseString = sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (callback != null) {
            if (!responseString.equals("")) {
                callback.sendVoteFinished(true, Integer.parseInt(responseString), vote);
            } else {
                callback.sendVoteFinished(false, 0, vote);
            }
            Log.d("SendVoteRunnable", "Response code:" + responseString);
        }
    }
}
