package picstac.cs48.androidapp.utils;

import android.content.Context;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Set;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;

/**
 * Created by nick on 3/7/15.
 * Utility class that handles serialization and deserialization of MyStac entries to disk.
 */
public class MyStacUtility {
    private static Set<String> ids;
    private static String FILENAME = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.mystac_local_save_filename);

    public static void addID(String id) {
        ids.add(id);
    }

    public static Set getIDS() {
        return new HashSet<String>(ids);
    }

    public static String printAll() {
        StringBuilder sb = new StringBuilder("");
        for (String item : ids) {
            sb.append(item);
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void save() {
        if (ids.size() < 1) return;

        try {
            FileOutputStream fos = Picstac.getInstance().getApplicationContext().openFileOutput(FILENAME, Context.MODE_PRIVATE);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(ids);

            oos.close();
            bos.close();
            fos.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void read() {
        try {
            FileInputStream fis = Picstac.getInstance().getApplicationContext().openFileInput(FILENAME);
            BufferedInputStream in = new BufferedInputStream(fis);
            ObjectInputStream ois = new ObjectInputStream(in);
            //noinspection unchecked
            ids = (HashSet<String>) ois.readObject();

            ois.close();
            in.close();
            fis.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            ids = new HashSet<String>();
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }
}
