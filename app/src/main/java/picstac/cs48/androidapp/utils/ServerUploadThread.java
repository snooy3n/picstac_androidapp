package picstac.cs48.androidapp.utils;

import android.graphics.Bitmap;
import android.net.http.AndroidHttpClient;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import picstac.cs48.androidapp.Picstac;
import picstac.cs48.androidapp.R;

/**
 * Thread that handles POSTing user-uploaded images to the server.
 */
public class ServerUploadThread extends Thread {
    public interface ServerUploadThreadCallback {
        public void serverPostThreadFinished(boolean timedOut);
    }

    private final int connectionTimeout = 1000 * 5; // timeout for establishing connection, 5 seconds
    private final int socketTimeout = 1000 * 10; // timeout for uploading media, 10 seconds

    private Bitmap                          bitmapToBeUploaded;
    private String                          caption;
    private ServerUploadThreadCallback      callback;
    private String[]                        latLonCoordinates;
    private String                          id;
    private boolean                         timedOut;

    public ServerUploadThread(Bitmap bitmap, String theCaption, ServerUploadThreadCallback callback, String latitude, String longitude, String userID) {
        bitmapToBeUploaded = bitmap;
        caption = theCaption;
        this.callback = callback;
        latLonCoordinates = new String[2];

        latLonCoordinates[0] = latitude;
        latLonCoordinates[1] = longitude;
        id = userID;

        timedOut = false;
    }

    /**
     * HTTP POST code to upload the photo to the server.
     */
    public void run() {
        try {
            String url = Picstac.getInstance().getApplicationContext().getResources().getString(R.string.add_api);
            HttpPost postRequest = new HttpPost(url);
            AndroidHttpClient httpClient = AndroidHttpClient.newInstance("");
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), connectionTimeout);
            HttpConnectionParams.setSoTimeout(httpClient.getParams(), socketTimeout);

            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
            reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("user", new StringBody(id, ContentType.TEXT_PLAIN));
            reqEntity.addPart("lat", new StringBody(latLonCoordinates[0], ContentType.TEXT_PLAIN));
            reqEntity.addPart("lon", new StringBody(latLonCoordinates[1], ContentType.TEXT_PLAIN));
            reqEntity.addPart("caption", new StringBody(caption, ContentType.TEXT_PLAIN));

            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmapToBeUploaded = Bitmap.createBitmap(bitmapToBeUploaded , 0, 0, bitmapToBeUploaded.getWidth(), bitmapToBeUploaded.getHeight());
                bitmapToBeUploaded.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                byte[] data = bos.toByteArray();
                ByteArrayBody bab = new ByteArrayBody(data, "test_image.jpg");
                reqEntity.addPart("image", bab);
            } catch(Exception e) {
                e.printStackTrace();
            }
            HttpEntity entity = reqEntity.build();
            postRequest.setEntity(entity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String serverResponse;
            StringBuilder responseBuilder = new StringBuilder();
            while((serverResponse = reader.readLine()) != null) {
                responseBuilder = responseBuilder.append(serverResponse);
            }
            Log.d("ServerUploadRunnable", "Server response: " + responseBuilder.toString());

        } catch(ConnectTimeoutException e) {
            timedOut = true;
        } catch(SocketTimeoutException e) {
            timedOut = true;
        } catch(Exception e) {
            timedOut = true;
            e.printStackTrace();
        }

        if (callback != null) {
            callback.serverPostThreadFinished(timedOut);
        }
    }
}
