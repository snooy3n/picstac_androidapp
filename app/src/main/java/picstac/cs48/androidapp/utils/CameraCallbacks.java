package picstac.cs48.androidapp.utils;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;

import java.io.File;
import java.io.FileOutputStream;

import picstac.cs48.androidapp.CameraActivity;
import picstac.cs48.androidapp.DisplayImageActivity;

/**
 * Callbacks executed by the camera implementation on specific actions.
 */
public class CameraCallbacks implements Camera.PictureCallback {

    private final Context context;
    private final int imgOrientation;

    public CameraCallbacks(Context context, int orientation) {
        this.context = context;
        this.imgOrientation = orientation;
    }

    /**
     * Callback from camera when photo is taken;
     * @param data the byte image data from the camera
     * @param camera the camera object that took the photo
     */
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        File photoFile = BitmapFileUtility.allocateImageFile(context);
        try {
            FileOutputStream fos = new FileOutputStream(photoFile);
            fos.write(data);
            fos.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        Intent startPreviewIntent = new Intent(context, DisplayImageActivity.class);
        startPreviewIntent.putExtra(CameraActivity.FILE_PATH, photoFile.getAbsolutePath());
        startPreviewIntent.putExtra(CameraActivity.ORIENTATION, imgOrientation);
        context.startActivity(startPreviewIntent);
    }
}
