package picstac.cs48.androidapp.utils;

import android.util.Log;

import picstac.cs48.androidapp.Picstac;

/**
 * Created by Nick on 2/19/2015.
 * Simple thread that clears the disk and memory bitmap cache.
 */
public class TrimCacheMemoryThread extends Thread {
    @Override
    public void run() {
        Log.e("TrimCacheMemoryThread", "Trimming cache memory!");
        Picstac.getInstance().getBitmapCache().trimMemory();
    }
}
