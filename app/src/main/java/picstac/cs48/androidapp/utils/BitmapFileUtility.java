package picstac.cs48.androidapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility class with static methods for allocating and resizing bitmaps
 */
public class BitmapFileUtility {

    /**
     * Allocates a new bitmap file in the Android filesystem.
     * @param context an application context
     * @return a File object pointing to the newly allocated file, null if the allocation fails
     */
    public static File allocateImageFile(Context context) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "img_" + timeStamp;
        File storageDirectory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image;

        try {
            image = File.createTempFile(fileName, ".jpg", storageDirectory);
        } catch(IOException e) {
            return null;
        }

        return image;
    }

    /**
     * Rotates a bitmap by a specified amount.
     * @param bitmap the bitmap to be rotated
     * @param rotationFactor the amount in degrees to rotate
     * @return the rotated bitmap
     */
    public static Bitmap rotateBitmap(Bitmap bitmap, float rotationFactor) {
        Matrix rotationMatrix = new Matrix();
        rotationMatrix.postRotate(rotationFactor);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                rotationMatrix, true);
    }

    /**
     * Calculates the correct sampling size for the requested width and height
     * @param options a suite of options for manipulating bitmaps
     * @param reqWidth the required width
     * @param reqHeight the required height
     * @return the sample factor
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height & width of the image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Returns the shorter of the two bitmap dimensions to use for square-cropping.
     * @param bitmap the bitmap to be cropped
     * @return the required dimension
     */
    public static int getCropDimensions(Bitmap bitmap) {
        if(bitmap.getWidth() >= bitmap.getHeight()) {
            return bitmap.getHeight();
        }else {
            return bitmap.getWidth();
        }
    }

    /**
     * Crops the requested bitmap to a square. Chops the longer dimension off to match the shorter.
     * @param bitmap the bitmap to be cropped
     * @return the cropped bitmap
     */
    public static Bitmap cropBitmapToSquare(Bitmap bitmap) {
        return ThumbnailUtils.extractThumbnail(bitmap, getCropDimensions(bitmap), getCropDimensions(bitmap));
    }

    /**
     * Resolves a bitmap's orientation by looking at exif metadata.
     * @param bitmapAbsPath the path to the bitmap in the filesystem
     * @return the orientation attribute
     * @throws IOException on error reading the file
     */
    public static int resolveBitmapOrientation(String bitmapAbsPath) throws IOException {
        ExifInterface exif = null;
        exif = new ExifInterface(bitmapAbsPath);
        return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
    }

    /**
     * Applies a direct orientation to a bitmap.
     * @param bitmap the bitmap to be re-oriented
     * @param orientation the desired orientation
     * @return the re-oriented bitmap
     */
    public static Bitmap applyOrientation(Bitmap bitmap, int orientation) {
        int rotate = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            default:
                return bitmap;
        }
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix m = new Matrix();
        m.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, m, true);
    }
}
