package picstac.cs48.androidapp.utils;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera camera;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        this.camera = camera;

        mHolder = getHolder();
        mHolder.addCallback(this);

        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    /**
     * Called on creation of a new preview SurfaceView.
     * @param holder the SurfaceHolder associated with the SurfaceView
     */
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size previewSize = null;

            // reversed because we are in portrait mode
            int targetPreviewWidth = getHeight();
            int targetPreviewHeight = getWidth();

            float targetRatio = targetPreviewWidth / (float) targetPreviewHeight;
            previewSize = computePreviewSize(parameters, targetRatio);

            parameters.setPreviewSize(previewSize.width, previewSize.height);
            camera.setParameters(parameters);
            camera.setPreviewDisplay(mHolder);
            camera.startPreview();
        } catch (IOException e) {
            Log.d("CameraPreview", "Failed to start preview.");
            e.printStackTrace();
        }
    }

    /**
     * Computes the correct aspect ratio for the preview on the user's device.
     * @param parameters the camera parameters
     * @param targetRatio the target ratio
     * @return the correct preview size
     */
    private Camera.Size computePreviewSize(Camera.Parameters parameters, float targetRatio) {
        Camera.Size previewSize = null;
        float closestRatio = Float.MAX_VALUE;
        List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();

        for (Camera.Size candidateSize : previewSizes) {
            float whRatio = candidateSize.width / (float) candidateSize.height;
            if (previewSize == null || Math.abs(targetRatio - whRatio) < Math.abs(targetRatio - closestRatio)) {
                closestRatio = whRatio;
                previewSize = candidateSize;
            }
        }
        return previewSize;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    /**
     * Callback for updating to the SurfaceView
     * @param holder the SirfaceView holder
     * @param format formatting integer used by Android
     * @param w width of view
     * @param h height of view
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (mHolder.getSurface() == null){
            return;
        }

        try {
            camera.stopPreview();
        } catch (Exception e){ }

        try {
            camera.setPreviewDisplay(mHolder);
            camera.startPreview();

        } catch (Exception e){
            Log.d("CameraPreview", "Failed to start preview: " + e.getMessage());
        }
    }
}
