package picstac.cs48.androidapp.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Lucas on 2/7/15.
 * Provides device location statistics.
 * Currently deprecated because we have switched over to Play Services location handling.
 * May still be useful, so it has not been deleted yet.
 */
public class DEPRECATED_LocationProvider implements LocationListener {

    private long                updateIntervalInMillis;
    private LocationManager     locationManager;
    private Location            currentLocation;

    public DEPRECATED_LocationProvider(Context context) {
        this(context, 1000 * 60 * 5); // default to 5 minutes
    }

    public DEPRECATED_LocationProvider(Context context, long updateIntervalInMillis) {
        this.updateIntervalInMillis = updateIntervalInMillis;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Log.d("Loc", "Current Loc: lat" + currentLocation.getLatitude() + ", lon" + currentLocation.getLongitude());
        enableUpdates();
    }

    /**
     * Enables the provider's updater. By default, it fetches new lat/lon coordinates for the
     * device every 5 minutes.
     */
    public void enableUpdates() {
        if(locationManager != null) {
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, updateIntervalInMillis, 0F, this);
            } catch (IllegalArgumentException e) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, updateIntervalInMillis, 0F, this);
            }
        }
    }

    /**
     * Pauses the provider's location update calls. Make sure to call this method whenever
     * you use DEPRECATED_LocationProvider in an activity that can go into the background. This is to prevent battery
     * drainage from regular GPS updates.
     */
    public void pauseUpdates() {
        if(locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    /**
     * Attempts to return the most recent latitude as a String value. If the
     * current location is invalid, returns null
     * @return the most recent latitude
     */
    public String getLatitude() {
        if(currentLocation != null) {
            return String.valueOf(currentLocation.getLatitude());
        }
        return null;
    }

    /**
     * Attempts to return the most recent longitude as a String value. If the
     * current location is invalid, returns null
     * @return the most recent longitude
     */
    public String getLongitude() {
        if(currentLocation != null) {
            return String.valueOf(currentLocation.getLongitude());
        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        Log.d("Loc", "onLocationChanged(Location) called");
        Log.d("Loc", "Current Loc: lat" + currentLocation.getLatitude() + ", lon" + currentLocation.getLongitude());
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
}
