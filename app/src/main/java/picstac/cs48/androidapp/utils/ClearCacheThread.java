package picstac.cs48.androidapp.utils;

import picstac.cs48.androidapp.posts.PostManager;
import uk.co.senab.bitmapcache.BitmapLruCache;

/**
 * Created by Nick on 2/14/2015.
 * Thread that clears entries from the mem/disk cache.
 */
public class ClearCacheThread extends Thread {
    public interface ClearCacheEvent {
        public void clearCacheEvent(boolean successful);
    }

    private PostManager pm;
    private BitmapLruCache cache;
    private ClearCacheEvent callback;

    public ClearCacheThread(PostManager pm, BitmapLruCache cache, ClearCacheEvent callback) {
        this.pm = pm;
        this.cache = cache;
        this.callback = callback;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < pm.getSize(); i++) {
                cache.remove(pm.getPost(i).getUrl());
            }
            cache.trimMemory();

            if (callback != null) {
                callback.clearCacheEvent(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (callback != null) {
                callback.clearCacheEvent(false);
            }
        }
    }
}
